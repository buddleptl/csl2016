%\subsubsubsection{Feature functions}
We use the same feature functions as in~\cite{kim2013}, described here
for completeness.
Some of the feature functions are quite general to sequence
recognition tasks, while some are tailored specifically to
fingerspelling recognition.   

\paragraph{DNN classifier-based feature functions} 
The first set of feature functions measure how well the frames within
a segment match the hypothesized label.  For this purpose we use the same
DNN classifiers as in the tandem model above.

Let $y$ be a FS-letter and $v$ be the value of a FS-letter or linguistic
feature, and $g(v|o_i)$ the softmax output of a DNN
classifier at frame $i$ for class $v$.
We define several feature functions based on aggregating the DNN outputs over a segment in various ways:
\begin{itemize}
  \item $\textrm{mean}$: $f^{\text{mean}}_{yv}(s, s', q, q', o)= \delta(s'=y)\cdot\frac{1}{q' - q + 1}\sum_{i=q}^{q'} g(v|o_i)$
  \item $\textrm{max}$: $f^{\text{max}}_{yv}(s, s', q, q', o)= \delta(s'=y)\cdot \max_{i \in \{q, q + 1, \dots, q'\}} g(v|o_i)$
  \item $\textrm{div$_s$}$: a concatenation of three $\textrm{mean}$ feature functions, each computed over a third of the segment
  \item $\textrm{div$_m$}$: a concatenation of three $\textrm{max}$ feature functions, each computed over a third of the segment
\end{itemize}
These are similar to features used in prior work on SCRFs for ASR with DNN-based feature functions~\cite{abdel2013deep,he2015segmental,tang2015}.
We tune the choice of aggregated feature functions on tuning data in our experiments.

\paragraph{Peak detection features}
A sequence of fingerspelled FS-letters yields a corresponding sequence of 
peak handshapes as described above.
The peak frame and the frames around it for each FS-letter tend to have
relatively little motion, while the transition frames between peaks
have much more motion.
To encourage each predicted FS-letter segment to have a
single peak, we define letter-specific ``peak detection features''
based on approximate derivatives of the visual descriptors.  We
compute the approximate derivative as the $l_2$ norm of the difference
between descriptors in every pair of consecutive frames, smoothed by
averaging over 5-frame windows.  Typically, there
should be a single local minimum in this derivative over the span of
the segment.  We define the feature function corresponding to FS-letter
$y$ as

\vspace{-.1in}
\[f_{y}^{\textrm{peak}}(s, s', q, q', o)=\delta(s'=y)\cdot\delta_{\textrm{peak}}(o, q, q')\]

\noindent where $\delta_{\textrm{peak}}(o, q, q')$ is $1$ if there is exactly one local minimum between time point $q$ and $q'$ and 0 otherwise.

\paragraph{Language model feature} 
The language model feature is a bigram probability of the FS-letter pair corresponding to an edge:
\[
f^{\text{lm}}(s, s', q, q', o)=p_{LM}(s, s').
\]
\noindent where $p_{LM}$ is our smoothed bigram language model.
%trained on ARPA CSR-III text.

\paragraph{Baseline consistency feature}
To take advantage of the already high-quality baseline that generated
the lattices, we use a baseline feature like the one in~\cite{zweig},
which is based on the 1-best output from the baseline tandem
recognizer.  The feature has value 1 when the corresponding segment
spans exactly one FS-letter label in the baseline output and the label matches it:
\[
f^{\text{bias}}(s, s', q, q', o)=\left\{
\begin{array}{rcl}
    +1 & \text{if}~C(q, q')=1, \\
    & \phantom{\text{if}} \text{and}~B(q, q')=s' \\
    -1 & \text{otherwise} \\
\end{array}
\right.
\]
where $C(q, q')$ is the number of distinct baseline labels in the time span from $q$ to $q'$, $B(q,q')$ is the label corresponding to time span $(q,q')$ when $C(q,q') = 1$.

