\section{Recognition methods}
\label{sec:method}

Our task is to take as input a video (a sequence of images)
corresponding to a fingerspelled word, as in Fig.~\ref{fig:ex}, and
predict the signed FS-letters.  This is a sequence prediction task
analogous to connected phone or word recognition, but there are some
interesting sign language-specific properties to the data domain.  For
example, one striking aspect of fingerspelling sequences, such as
those in Fig.~\ref{fig:ex}, is the large amount of motion and lack
of any prolonged ``steady state'' for each FS-letter.  As described in Sec.~\ref{sec:data}, each
FS-letter is represented by a brief ``peak of articulation'',
%of one or a few frames,
during which the hand's motion is at a minimum and the
handshape is the closest to the target handshape for the FS-letter.  This
peak is surrounded
by longer periods of motion between the current FS-letter and the
previous/next FS-letters.

Another striking property of sign language is the wide variation between signers.  Inspection of data such as Fig.~\ref{fig:ex} reveals some types of
signer variation, including differences in speed, hand appearance, and
non-signing motion before and after signing.  The speed variation is
large:  In our data, the ratio between the average per-letter duration is about $1.8$ between the fastest and slowest
signers. 

We consider
signer-dependent, signer-independent, and signer-adapted recognition.
We next describe
the recognizers we compare, as well as the techniques we explore for
signer adaptation.  All of the recognizers use deep neural network
(DNN) classifiers of FS-letters or handshape features.

\subsection{Recognizers}
In designing recognizers, we keep several considerations in mind.
First, the data set, while large in comparison to prior fingerspelling
data sets,
is still quite small compared to typical speech data sets.  This means
that large models with many context-dependent units are infeasible to
train on our data (as confirmed by our initial experiments).  We
therefore restrict attention here to ``mono-letter'' models, that is
models in which each unit is a context-independent FS-letter.  We also
consider the use of articulatory (phonological and phonetic) feature
units, and there is evidence from speech recognition research that
these may be useful in low-data
settings~\cite{stuker2003integrating,cetin}.  Second, we would
like our models to be able to capture detailed sign language-specific
information, such as the dynamic aspects of FS-letters as
discussed above; this suggests the segmental models that we consider
below.  Finally, we would like our models to be easy to adapt to new
signers.  In order to enable this, all of our recognizers use independently trained deep
neural network (DNN) classifiers, which can be adapted and plugged
into different sequence models.  Our DNNs are trained using an
L2-regularized cross-entropy loss.  The inputs are the image features
concatenated over a multi-frame window centered at the current frame, 
which are fed through several fully connected layers followed by a
softmax output layer with as many units as labels.\footnote{We have also considered convolutional neural networks with
  raw image pixels as input, but these did not significantly outperform the DNNs on image features.  See Sec.~\ref{sec:cnn-exp}.}

\subsubsection{Tandem model}
The first recognizer we consider is a fairly typical tandem model~\cite{Her00,grezl2007probabilistic}.  Frame-level
image features are fed to seven DNN classifiers, one of which predicts
the frame's FS-letter label and six others which predict handshape
phonological features.  The phonological features are defined in Tab.~\ref{t:features}, and example frames for values of one feature
are shown in Fig.~\ref{f:feature_ex}.
The classifier outputs and image features are reduced in
dimensionality via PCA and then concatenated.  The concatenated features
form the observations in an HMM-based recognizer with Gaussian mixture
observation densities.  We use a 3-state HMM for each
(context-independent) FS-letter, plus one HMM each for initial and final
``silence'' (non-signing segments).

\begin{table}[t]
\begin{center}
\begin{tabular}{|l|l|}
\hline
{\bf Feature} & {\bf Definition/Values}  \\ \hline \hline
SF point of & side of the hand where \\
reference & SFs are located  \\ \cline{2-2}
(POR) & {\it SIL, radial, ulnar, radial/ulnar} \\ \hline\hline
SF joints & degree of flexion or \\
& extension of SFs  \\  \cline{2-2}
& {\it SIL, flexed:base, flexed:nonbase,} \\ & {\it flexed:base \& nonbase,} \\
& {\it stacked, crossed, spread} \\ \hline\hline
SF quantity &combinations of SFs \\ \cline{2-2}
& {\it N/A, all, one,} \\ & {\it one $>$ all, all $>$ one} \\ \hline\hline
SF thumb & thumb position  \\ \cline{2-2}
& {\it N/A, unopposed, opposed} \\ \hline\hline
SF handpart & internal parts of the hand  \\ \cline{2-2}
& {\it SIL, base, palm, ulnar} \\ \hline\hline
UF & open/closed \\ \cline{2-2}
& {\it SIL, open, closed} \\ \hline
\end{tabular}
\caption{Definition and possible values for phonological features
  based on~\cite{bren}.  The first five features are properties of the
  active fingers ({\it selected fingers}, SF); the last feature is the
  state of the inactive or {\it unselected fingers} (UF).  In addition
  to Brentari's feature values, we add a SIL (``silence'') value to
  the features that do not have an N/A value.  For detailed
  descriptions, see~\cite{bren}.}
\label{t:features}
\end{center}
\end{table}

\begin{figure}[h!]
\begin{center}
        \hspace{-.5em}\includegraphics[width=0.65\linewidth]{feature_ex.pdf}
\end{center}
\vspace{-.25in}
\caption{Example images corresponding to SF thumb = 'unopposed' (upper row) and SF thumb = 'opposed' (bottom row).}
\label{f:feature_ex}
\end{figure}

In addition, we use a bigram letter language model.  In general the language
model of FS-letter sequences is difficult to define or
estimate, since fingerspelling does not follow the same distribution
as English words and there is no large database
of natural fingerspelled sequences on which to train. In addition, in our
data set, the words were selected so as to maximize
coverage of letter n-grams and word types rather than
following a natural distribution.  For this work, 
the language model is trained using ARPA CSR-III text, which
includes English words and names~\cite{csriii}.  
The issue of language modeling for fingerspelling deserves more
attention in future work.

\subsubsection{Segmental CRF}
The second recognizer is a segmental CRF (SCRF).  SCRFs~\cite{sarawagisemi,zweig} are conditional log-linear models with feature functions that can be based on variable-length segments of input frames, allowing for great flexibility in defining feature functions.  
Formally, for a sequence of frames $o_1, o_2, \dots, o_T$, a segmentation of size $k$ is
a sequence of time points $0 = q_0, q_1, \dots, q_{k-1}, q_k = T$ used to denote
time boundaries of segments.  In other words, the $i$-th segment
starts at time $q_{i-1}$ and ends at $q_i$.
The labeling of segmentation $q$ is a sequence of labels
$s_1, s_2, \dots, s_k$.
We will denote the length
of label sequences and segmentations $|s|$, $|q|$, respectively.
For a segmentation of size $k$, $|s| = |q| = k$.
A SCRF defines,
over a sequence of labels $s$ given a sequence of frames $o$,
a probability distribution
\begin{equation*}
p(s | o) = \frac{\sum_{q: |q| = |s|} \exp\left(\sum_{i=1}^{|q|}
        \boldsymbol{\lambda}^\top \mathbf{f}(s_{i-1}, s_i, q_{i-1}, q_i, o)\right)}{
    \sum_{s'} \sum_{q': |q'| = |s'|} \exp\left(\sum_{i=1}^{|q'|}
        \boldsymbol{\lambda}^\top \mathbf{f}(s'_{i-1}, s'_i, q'_{i-1}, q'_i, o)\right)}
\end{equation*}
where $\boldsymbol{\lambda}$ is a weight vector, and $\mathbf{f}(s, s', q, q', o)$ is
a feature vector.
We assume $s_0$ is the sentence-start string, so that $\mathbf{f}(s_0, s_1, q_0, q_1)$ is well-defined.
In the case of sign language, it is natural to define feature functions that are sensitive to the duration and dynamics of each FS-letter segment.  

\subsubsection{Rescoring segmental CRF}
\label{sec:re_scrf}
One common way of using SCRFs is to rescore the outputs from
a baseline HMM-based recognizer, and this is one way we apply SCRFs here.
We first use the baseline recognizer, in this case the tandem
HMM, to generate lattices of high-scoring segmentations and labelings,
and then rescore them with our SCRFs.

\input{features}

\subsubsection{First-pass segmental CRF}
One of the drawbacks of a rescoring approach is that the quality of the
final outputs depends both on the quality of the baseline lattices and
the fit between the segmentations in the baseline lattices and those
preferred by the second-pass model.  We therefore also consider a
first-pass segmental model, using similar features to the rescoring model.
The difference between a rescoring model and a first-pass model
lies in the set of segmentations and labellings to marginalize over.
The rescoring model only marginalizes over the hypotheses generated
by the tandem model, while the first-pass model marginalizes over
all possible hypotheses.  The first-pass model thus does not
depend on the tandem HMM.
We use a first-pass SCRF inspired by the phonetic
recognizer of Tang {\it et al.}~\cite{tang2015},
and the same feature functions as
in~\cite{tang2015}, namely average DNN outputs over each segment,
samples of DNN outputs within the segment, boundaries of DNN outputs
in each segment, duration and bias.
Recall that $g(v|o_i)$ is the softmax output of a DNN classifier
at frame $i$ for class $v$.  We use the same DNN frame
classifiers as in the rescoring setting.
Also recall that $y$ is a FS-letter and $v$ is the value of a FS-letter or linguistic feature.
The exact definition of each feature is listed below.

\paragraph{Averages of DNN outputs}
These are the same as the mean features used in the rescoring setting.

\paragraph{Samples of DNN outputs} These are samples of the DNN outputs at the mid-points
of three equal-sized sub-segments
\begin{align*}
f_{yvi}^{\text{spl}}(s, s', q, q', o) = \delta(s' = y) g(v | o_{q + (q' - q) i})
\end{align*}
for $i = 16\%, 50\%, 84\%$.

\paragraph{Left boundary} Three DNN output vectors around the left boundary of the segment
\begin{align*}
f_{yvk}^{\text{l-bndry}}(s, s', q, q', o) = \delta(s' = y) g(v | o_{q+k})
\end{align*}
for $k \in \{-1, 0, 1\}$.

\paragraph{Right boundary} Three DNN output vectors around the right boundary of the segment
\begin{align*}
f_{yvk}^{\text{r-bndry}}(s, s', q, q', o) = \delta(s' = y) g(v | o_{q'+k})
\end{align*}
for $k \in \{-1, 0, 1\}$.

\paragraph{Duration} An indicator for the duration of a segment
\begin{align*}
f_{yk}^{\text{dur}}(s, s', q, q', o) = \delta(q' - q = k) \delta(s' = y)
\end{align*}
for $k \in \{1, 2, \dots, 30\}$.

\paragraph{Bias} A constant 1
\begin{align*}
f^{\text{bias}}_y(s, s', q, q', o) = 1 \cdot \delta(s' = y).
\end{align*}

As shown in the above definitions, all features are lexicalized with the unigram label,
i.e., they are multiplied by an indicator for the hypothesized FS-letter label.

%frame-avg@1,frame-samples@1,left-boundary@1,right-boundary@1,length-indicator@1,bias@1
\input{dnn}
