\subsection{A semi-Markov CRF approach}
\label{sec:methods}

We begin by defining the problem and our notation.  Let the sequence of visual observations for a given video (corresponding to a single word) be $O = o_1, \ldots, o_T$, \karenedit{where each $o_t$ is a multidimensional image descriptor for frame $t$}.  Our goal is to predict the label (letter) sequence.  \karenedit{Ideally we would like to predict the best label sequence, marginalizing out different possible label start and end times, but in practice we use the typical approach of predicting the best sequence of frame labels $S = s_1, \ldots, s_T$}.  We predict $S$ by maximizing its conditional probability under our model, $\hat{S} = \text{argmax}_S P(S|O)$.
In generative models like HMMs, we have a joint model $P(S,O)$ and we make a prediction using Bayes' rule.
In conditional models we directly represent the conditional distribution $P(S|O)$.  For example, in a typical linear-chain CRF, we have:
\[
p(S|O) = \frac{1}{Z(O)}\exp\left(\sum_{v,k}\lambda_k f_k(S_v,O_v) + \sum_{e,k}\mu_k g_k(S_e)\right)
\]
where $Z(O)$ is the partition function, $f_k$ are the ``node'' feature
functions that typically correspond to the state in a single frame
$S_v$ and its corresponding observation $O_v$, $g_k$ are ``edge''
feature functions corresponding to inter-state edges, \karenedit{$e$
  ranges over pairs of frames and $S_e$ is the pair of states
  corresponding to $e$}, and $\lambda_k$ and $\mu_k$ are the 
%corresponding 
weights.  
%In a linear-chain CRF, the conditional distribution factorizes over individual frames; that is, $v$ ranges over the frames and $e$ over the sequence of frame pairs.

It may be more natural to consider feature functions that span entire
segments corresponding to the same label.  Semi-Markov
CRFs~\cite{sarawagisemi}, also referred to as segmental
CRFs~\cite{zweig} or SCRFs, provide this ability.  
%Figure~\ref{f:SCRF} shows a SCRF for fingerspelled letter recognition, with an example labeling and segmentation.

%\begin{figure}[t]
%\centering
%\includegraphics[width=0.45\textwidth]{figs/SCRF2_small.pdf}
%\caption{SCRF for fingerspelled letter recognition, with an example fingerspelled letter sequence '-V-I-T-A-'.  Dotted lines show one possible segmentation.  Feature functions are defined over edges between labels and over segments of tobservations.}
%\label{f:SCRF}
%\end{figure}

\karenedit{Figure~\ref{fig:s-crf} illustrates the SCRF notation, which we now describe.}
In a SCRF, we consider the segmentation to be a latent variable and sum over all possible segmentations of the observations corresponding to a given label sequence to get the conditional probability of the label sequence \karenedit{$S = s_1, \ldots, s_L$, where the length of $S$ is now the (unknown) number of distinct labels $L$}:
\[
p(S|O) = \frac{\sum_{q ~\text{s.t.}~ |q|=|S|} \exp\left(\sum_{e \in q,k} \lambda_k f_k (s_l^e,s_r^e,O_e)\right)}{\sum_{S^\prime} \sum_{q~\text{s.t.} |q|=|S^\prime|} \exp\left(\sum_{e \in q,k} \lambda_k f_k (s_l^e,s_r^e,O_e)\right)}
\]
Here, $S'$ ranges over all possible state (label) sequences, $q$ is a segmentation of the observation sequence whose length (number of segments) must be the same as the number of states in $S$ \karenedit{(or $S^\prime$)}, $e$ ranges over all state pairs in $S$, $s_l^e$ is the state which is on the left of an edge, $s_r^e$ is the state on the right of an edge, and $O_e$ is the \karenedit{multi-frame} observation segment associated with \karenedit{$s_r^e$}.
%the state on the right of the edge $e$.  
In our work, we use a baseline frame-based recognizer to generate a set of candidate segmentations of $O$, and sum only over those candidate segmentations.  In principle the inference over all possible segmentations can be done, but typically this is only feasible for much smaller search spaces than ours. 

\begin{comment}\subsection{Letter model and transition model}
In the formulation above, each state could represent either letter or transition of two letter pairs. For each case, we need to define different feature functions.
\subsection{Feature functions for letter model}
\end{comment}



\begin{figure*}
%  \begin{minipage}[c]{.27\linewidth}
%    \includegraphics[width=0.99\textwidth]{figs/asl-alphabet-wiki}
%  \end{minipage}\hspace{2em}%
%  \begin{minipage}[c]{.68\linewidth}
  \centering
  \begin{tikzpicture}[scale=.6]
    % frames
    \foreach \t in {1,2,3,6,7,8,9,12,13}{
      \draw[fill=gray] (\t,0) rectangle ++(.8,-3);
      \node at (5,-1.5) {\large \ldots};
      \node at (11,-1.5) {\large \ldots};
      \node at (15,-1.5) {\large \ldots};
    }
    % observations
    \node at (1.4,-3.4) {$o_1$};
    \node at (2.4,-3.4) {$o_2$};
    \node at (3.4,-3.4) {$o_3$};
    \node[text width=7em] (labelvis) at (-1,-2) {visual\\feature\\vectors};
    \coordinate (s1) at (1,1.5);
    \coordinate (e1) at (7.8,1.5);
    \coordinate (s2) at (8,1.5);
    \coordinate (e2) at (12.8,1.5);
    \coordinate (s3) at (13,1.5);
    % segments
    \draw[thick,rounded corners] ($(s1) - (0,.5)$) -- ++(0,.5) --
    (e1)
    node[midway,circle,fill=black,inner sep=2pt,name=seg1] {}  -- ++(0,-.5);
    \draw[thick,rounded corners] ($(s2) - (0,.5)$) -- ++(0,.5) -- (e2)
    node[midway,circle,fill=black,inner sep=2pt,name=seg2] {} -- ++(0,-.5);
    \draw[thick,rounded corners] ($(s3) - (0,.5)$) -- ++(0,.5) -- ++(2,0)
    node[midway,circle,fill=black,inner sep=2pt,name=seg3] {};
    % observation sequences
    \draw[decorate,decoration={brace,amplitude=5pt}] ($(e1) - (0,5)$)
    -- ($(s1) - (0,5)$) node [midway,below=4pt] {$O_{(1)}$};
    \draw[decorate,decoration={brace,amplitude=5pt}] ($(e2) - (0,5)$)
    -- ($(s2) - (0,5)$) node [midway,below=4pt] {$O_{(2)}$};
    % states
    \node[draw] (state1) at ($(seg1) + (0,2)$) {$s_1$};
    \node[draw] (state0) at ($(state1) - (4,0)$) {$s_0$};
    \node[draw] (state2) at ($(seg2) + (0,2)$) {$s_2$};
    \node[draw] (state3) at ($(seg3) + (0,2)$) {$s_3$};
    \path let \p1=(labelvis), \p2=(state0) in node[text width=7em]  at(\x1,\y2)
 {state\\labels};
    \draw (state1) -- (seg1);
    \draw (state2) -- (seg2);
    \draw (state3) -- (seg3);
    \path let \p1=(labelvis), \p2=(s1) in node[text width=7em]
    at(\x1,\y2)  {segmentation $q$};
    % edges
    \draw (state0) -- (state1) node[midway,below] {$e_1$};
    \draw (state1)--(state2) node[midway,below] {$e_2$};
    \draw (state2)--(state3) node[midway,below] {$e_3$};
    \draw (state3) -- ++(1.5,0);
    % states per edge
    \path (state0) -- ++(0,1) node {$s_l^{(1)}$};
    \path (state1) -- ++(0,1) node {$s_r^{(1)}=s_l^{(2)}$};
    \path (state2) -- ++(0,1) node {$s_r^{(2)}=s_l^{(3)}$};
    % times
    {\tiny
    \node at (1.5,.5) {$t(1)$};
    \node at (7.5,.5) {$T(1)$};
    \node at (8.5,.5) {$t(2)$};
    \node at (12.5,.5) {$T(2)$};
    \node at (13.5,.5) {$t(3)$};
  }
  \end{tikzpicture}
 % \end{minipage}

%  \begin{minipage}[t]{.27\linewidth}
%    \caption{The ASL fingerspelled alphabet. All of the
%  letters except for J and Z are static. [From Wikipedia]}
%\label{f:alphabet}
%  \end{minipage}\hspace{2em}%
%  \begin{minipage}[t]{.68\linewidth}
    \caption{Illustration of SCRF notation. For example, edge $e_2$ is
associated with \karenedit{the ``left state'' $s_l^{(2)}$, the ``right state'' $s_r^{(2)}$, and the 
    segment of observations $O_{(2)}$ spanning frames $t(2)$ through
    $T(2)$.} 
%\karencomment{[Changed notation for O in fig. to match
%equations. --KL]}
}
%; the observation     sequence for this segment is $O_b=(o_{t(2)},\ldots,o_{T(2)})$.}
% The   segment corresponds to state $s_r^{(2)}=s_2$, following state $s_l^{(2)}=s_1$.}
  \label{fig:s-crf}
%  \end{minipage}
\end{figure*}



\subsection{Feature functions}
We define several types of feature functions, some of which are quite general to \karenedit{sequence recognition tasks and some of which are tailored to fingerspelling recognition}:

\subsubsection{Language model \karenedit{feature}} 
%\karencomment{[This means we are only using a global LM and not a local one -- correct? --KL ]} \taehwancomment{[Yes, it's a global LM.]}
The language model \karenedit{feature is a smoothed bigram probability} of the letter pair corresponding to an edge:
\[
f_{lm}(s_l^e,s_r^e,O_e)=p_{LM}(s_l^e,s_r^e).
\]
%\noindent where $p_{LM}$ is a smoothed bigram language model trained by using ARPA CSR-III text, which includes English words and names~\cite{csriii}.


\subsubsection{Baseline consistency feature}
To take advantage of the existence of a high-quality baseline, we use a baseline feature like the one introduced by~\cite{zweig}.  This feature is constructed using the 1-best output hypothesis from an HMM-based baseline recognizer.  The feature value is 1 when a segment spans exactly one letter label hypothesized by the baseline and the label matches it:
\[
f_{b}(s_l^e,s_r^e,O_e)=\left\{
\begin{array}{rcl}
    +1 & \text{if}~C(t(e),T(e))=1, \\
    & \phantom{\text{if}} \text{and}~B(t(e),T(e))=w(s_r^e) \\
    -1 & \text{otherwise} \\
\end{array}
\right.
\]
where $t(e)$ and $T(e)$ are the start and end times corresponding to edge $e$, $C(t,T)$ is the number of distinct baseline labels in the time span from $t$ to $T$, $B(t,T)$ is the label corresponding to time span $(t,T)$ when $C(t,T) = 1$, and $w(s)$ is the letter label of state $s$.

\subsubsection{Handshape classifier-based feature functions} 
The next set of feature functions measure the degree of match between the intended segment label and the appearance of the frames within the segment.  For this purpose we use a set of frame classifiers, each of which classifies either letters or linguistic handshape features.
As in \cite{kim}, we use the linguistic handshape feature set developed by Brentari~\cite{bren}, who proposed seven features to describe handshape in ASL.  Each such linguistic feature (not to be confused with feature functions) has 2-7 possible values.  Of these, we use the six that are contrastive in fingerspelling. See
Table~\ref{t:features} for the details.
%\cite{kim} used Multi-layer Perceptron (MLP) to train classifiers for labels of phonological features with inputs of image features. Similarly, we also train MLP and incorporate the outputs of MLP by defining the following phonological features. \\
For each linguistic feature or letter, we train a classifier that produces a score for each feature value for each video frame.  We also train a separate letter classifier.  We use neural network (NN) classifiers, and consider several types of NN output-layer functions as the classifier scores:
\paragraph{NN output layer types}
\begin{itemize}
  \item linear: $g(v|x)=w^\text{T}_v\phi(x)$
%  \[
%w^\text{T}_c\phi(x), \frac{w^\text{T}_c\phi(x)}{\sum_i w^\text{T}_i \phi(x)}, \frac{1}{1 + w^\text{T}_c\phi(x)}
%\] 

  \item softmax: $g(v|x)=\frac{\exp(w^\text{T}_v\phi(x))}{\sum_i \exp(w^\text{T}_i \phi(x))}$
  \item sigmoid: $g(v|x)=\frac{1}{1 + \exp(-w^\text{T}_v\phi(x))}$
\end{itemize}
where $x$ is the input to the NN (in our case, the visual descriptors concatenated over a window around the current frame), $w_v$ is the weight vector in the last layer of the NN corresponding to linguistic feature $v$, and $\phi(x)$ is the input to the last layer.  We then use these score functions $g(v|x)$ to define four types of segment feature functions:\\

\paragraph{\bf Feature functions}
Let $y$ be a letter and $v$ be the value of a linguistic feature or 
letter, \karenedit{$N_{e} = |O_e| = T(e)+1-t(e)$} the length of \karenedit{an observation segment corresponding to edge $e$}, and
\karenedit{$g(v|o_i)$ the output of a NN classifier at frame $i$ corresponding
to class $v$}. We define  
\begin{itemize}
  \item $\textrm{mean}$: $f_{yv}(s_l^e,s_r^e,O_e)= \\ \delta(w(s_r^e)=y)\cdot\frac{1}{\karenedit{N_{e}}}\sum_{i=t(e)}^{T(e)} g(v|o_i)$
  \item $\textrm{max}$: $f_{yv}(s_l^e,s_r^e,O_e)= \\ \delta(w(s_r^e)=y)\cdot \max_{i \in (t(e),T(e))} g(v|o_i)$
  \item $\textrm{div$_s$}$: a concatenation of three $\textrm{mean}$ feature functions, each computed over a third of the segment
  \item $\textrm{div$_m$}$: a concatenation of three $\textrm{max}$ feature functions, each computed over a third of the segment
\end{itemize}
%where $y$ is a letter, $v$ is the value of a linguistic feature or a letter, %$N_{s^\prime} = et - st$ is the length of the segment, and $g(v|o_i)$ is %the output of a NN classifier at frame $i$ corresponding to class $v$. 
%Thus this is the average of the function $g(\cdot)$ of phonological feature value and we could use maximum rather than the average as well.  

%The features and their values are given in Table~\ref{t:features}. \cite{kim} used Multi-layer Perceptron (MLP) to train classifiers for labels of phonological features with inputs of image features. Similarly, we also train MLP and incorporate the outputs of MLP by defining the following phonological features:
\begin{comment}
\subsubsection{Letter posterior}
Similarly as phonological features, we use output of letter MLP in \cite{kim}, letter posterior, and the same output types of MLP and kinds of feature functions. For example,
\begin{itemize}
  \item $mean$: $f_{yx}(s,s^\prime,o^{et}_{st})=\delta(w(s^\prime)=y)\cdot\frac{1}{N_{s^\prime}}\sum_{i=1}^{N_{s^\prime}} g(x|o_i)$
%  \item $max$: $f_{yv}(s,s^\prime,o^{et}_{st})=\delta(w(s^\prime)=y)\cdot \max_{i \in s^\prime} g(v|o_i)$
%  \item $div_s$: we equally divide a segment into three and for each sub-segment use the definition of $mean$.
%  \item $div_m$: we equally divide a segment into three and for each sub-segment use the definition of $max$.
%  \item $peak_s$: we pick the frame whose value is a maximum and use some small interval around it to define the feature function. Then we use $mean$ for that small sub-segment.
\end{itemize}
where $y$ is a letter on a state in SCRF model and $x$ is a letter value in the letter MLP. 
\end{comment}

\subsubsection{Peak detection features} \label{sec:peaks}
%The peak frame and the frames around it for each letter tend to be characterized %by very little motion, whereas the transitional frames between letter peaks %have more motion.  
Fingerspelling a sequence of letters yields a corresponding sequence of 
``peaks'' of
articulation. Intuitively, these are frames in which the hand reaches the
target handshape for a
particular letter.
The peak frame and the frames around it for each letter tend to be
characterized by very little motion as the transition to the current
letter has ended while the transition to the next letter has not yet begun,
whereas the transitional frames between letter peaks have more motion. To use this information and encourage each \karenedit{predicted} letter segment to have a single peak, we define letter-specific ``peak detection features'' as follows.
We first compute approximate derivatives of the visual descriptors, consisting of the $l_2$ norm of the difference between descriptors in every pair of consecutive frames, smoothed by averaging over 5-frame windows.  We 
%then define the feature function value as 1 if the corresponding segment includes 
\karenedit{expect there to be} a single local minimum in this approximate derivative function over the span of the segment.  Then we define the feature function corresponding to each letter $y$ as

\vspace{-.1in}
\[f_{y}^{\textrm{peak}}(s_l^e,s_r^e,O_e)=\delta(w(s_r^e)=y)\cdot\delta_{\textrm{peak}}(O_e)\]

\noindent where $\delta_{\textrm{peak}}(O_e)$ is $1$ if there is only one local minimum in the segment $O_e$ and 0 otherwise.

\begin{comment}
\subsection{Feature functions for transition model}
Similarly as the letter model, we use language model feature and baseline feature.
\subsubsection{Language model feature} 
Different from the letter model, we use uniform unigram language model as feature at this moment due to the lack of training data. That is, in training data, we do not have all possible letter pairs. Also with this uniform language model, weights to them could be learned in training. We still might be able to use previous used bigram language model (from online dictionary) as unigram language model feature here because bigram is encoded in unigram in this letter transition model even though we do not exploit it here yet.
\[
f_{lm}(s,s^\prime,o^{et}_{st})=LM(s^\prime)=\frac{1}{N}
\]
where $N$ is the number of all possible letter pairs.

\subsubsection{Baseline feature}
To have baseline output for our letter transition model, we could use either letter HMM baseline or transition HMM baseline. That is, we might be able to train HMM baseline with letter transition labels. But since we do not have all possible letter pairs in our training data (even with test data), at this moment we use letter HMM baseline and then concatenate letters in output to generate transition pairs.
\[
f_{b}(s,s^\prime,o^{et}_{st})=\left\{
\begin{array}{rcl}
    +1 & if & C(st,et)=1, B(st,et)=w(s^\prime) \\
    -1 & if & \text{otherwise} \\
\end{array}
\right.
\]
where $C(st,et)$ is the number of baseline detection in a timespan from $st$ to $et$, $B$ is the label when $C(\cdot)$ is 1 and $w(\cdot)$ is a letter pair label of a state.


\subsubsection{Phonological feature}
For each phonological feature value, we would like to define two feature functions so that the one focuses on the part of the first half in a transition segment and the other focuses on the second half. To do this, we define two types of kernels, uniform and Gaussian kernel. Uniform kernel put the same weights for all frames but Gaussian kernel put more weights to the frames nearer to the both ends of a segment. 
\paragraph{\bf Feature function with the uniform kernel}  
\begin{itemize}
  \item $first~half$: $f_{y y^\prime v}^{u1}(s,s^\prime,o^{et}_{st})=\delta(w(s^\prime)=y y^\prime)\cdot\frac{1}{N_1}\sum_{i=0}^{N_1-2} g(v|o_{st+i})$
  \item $second~half$: $f_{y y^\prime v}^{u2}(s,s^\prime,o^{et}_{st})=\delta(w(s^\prime)=y y^\prime)\cdot\frac{1}{N_2}\sum_{i=0}^{N_2-1} g(v|o_{\frac{et+st}{2}+i})$
\end{itemize}
where $y$ and $y^\prime$ are letters in a transition pair, $v$ is the value of phonological feature, $N_1=\frac{et+st}{2}-st$ is the length of a segment $o^{\frac{et+st}{2}-1}_{st}$, $N_2=et-\frac{et+st}{2}+1$ is the length of a segment $o^{et}_{\frac{et+st}{2}}$, and $g(\cdot|o_i)$ is the output of MLP at the frame $i$. Note that if $\frac{et+st}{2}$ is not an integer, we round it up. For Gaussian kernel, similarly,

\paragraph{\bf Feature function with the Gaussian kernel}  
\begin{itemize}
  \item $f_{y y^\prime v}^{g1}(s,s^\prime,o^{et}_{st})=\delta(w(s^\prime)=y y^\prime)\cdot W_1(\frac{et+st}{2}-st)^\top G(v|st,\frac{et+st}{2}-1)$
  \item $f_{y y^\prime v}^{g2}(s,s^\prime,o^{et}_{st})=\delta(w(s^\prime)=y y^\prime)\cdot W_2(et-\frac{et+st}{2}+1)^\top G(v|\frac{et+st}{2},et)$
\end{itemize}
where $W_1(x)=\frac{1}{||W^\prime_1(x)||_2}W^\prime_1(x)$ and $W^\prime_1(x)=\frac{1}{p_n(0)}[p_n(0),p_n(1),\cdots,p_n(x-1)]^\top$ and $p_n(\cdot)$ is the normal density function with mean $0$ and standard deviation $c\cdot x$ with some constant $c$ like $0.4$, $W_2(x)=\frac{1}{||W^\prime_2(x)||_2}W^\prime_2(x)$ and $W^\prime_2(x)=\frac{1}{p_n(0)}[p_n(x-1),p_n(x-2),\cdots,p_n(0)]^\top$, and  $G(v|x,y)=[g(v|x),g(v|x+1),\cdots,g(v|y)]^\top$. Note that $||\cdot||_2$ is L2-norm.


\subsubsection{Letter posterior}
Similarly as phonological features, we use output of letter MLP in \cite{kim}, letter posterior, and the same output types of MLP and kinds of feature functions. For example,
\paragraph{\bf Feature function with the uniform kernel}  
\begin{itemize}
  \item $first~half$: $f_{y y^\prime x}^{u1}(s,s^\prime,o^{et}_{st})=\delta(w(s^\prime)=y y^\prime)\cdot\frac{1}{N_1}\sum_{i=0}^{N_1-2} g(x|o_{st+i})$
\end{itemize}
where $x$ is a letter value in the letter MLP. 
  
  
\subsubsection{Trajectory feature}
It might be interesting and useful to use information of how much rapidly motions in each frame is changing for each letter transition. Thus first we get approximate derivates as the differences of SIFT feature in two consecutive frames for the degree of motion changes. Then we have averages of them for each letter pairs in training data and normalize it with respect to the fixed temporal length. Finally, we calculate how much the new unseen test data matches our \emph{template} and use it as a feature. That is,
\[
f_{y y^\prime}^{traj}(s,s^\prime,o^{et}_{st})=\delta(w(s^\prime)=y y^\prime)\cdot \frac{D^\prime(o^{et}_{st})^\top}{||D^\prime(o^{et}_{st})||_2} \cdot \frac{Q(y, y^\prime)}{||Q(y, y^\prime)||_2}
\]
where $T$ is a fixed length,$D(o^{et}_{st})=[d(st),\cdots,d(et-1)]^\top$ and $D^\prime(\cdot)$ is interlaced vector of $D(\cdot)$ with length $T$,    $d(i)=||o_{i+1}-o_i||_2^2$, and $Q(y, y^\prime)$ is the average motion for letter pair $y, y^\prime$. Thus, perfect matching would have 1 for this feature value. Note that some letter pairs are missing in training data and in these case we use average over all letter pairs in training data instead. 

\subsubsection{Duration feature}
Finally, it might be reasonable to consider a too long or short segment as unlikely compared to the average duration of that letter pair. Therefore, we introduce duration feature as average duration of a specific letter pair and its value is close to 1 when the length of segment is close to the average. 
\[
f_{y y^\prime}^{du}(s,s^\prime,o^{et}_{st})=\delta(w(s^\prime)=y y^\prime)\cdot p_n(avg(y y^\prime)-(et-st+1))/p_n(0) 
\]
where $avg(y y^\prime)$ is the average duration of letter pair $y y^\prime$ and $p_n(\cdot)$ is normal density function with mean $0$ and some constant standard deviation $c$.    
\end{comment}
\begin{comment}
\begin{table}[t]
\begin{center}
\begin{tabular}{|l|l|}
\hline
{\bf Feature} & {\bf Definition/Values}  \\ \hline \hline
SF point of & side of the hand where \\ 
reference & SFs are located  \\ \cline{2-2} 
(POR) & {\it SIL, radial, ulnar, radial/ulnar} \\ \hline\hline
SF joints & degree of flexion or \\
& extension of SFs  \\  \cline{2-2}
& {\it SIL, flexed:base, flexed:nonbase,} \\ & {\it flexed:base \& nonbase,} \\
& {\it stacked, crossed, spread} \\ \hline\hline
SF quantity &combinations of SFs \\ \cline{2-2}
& {\it N/A, all, one,} \\ & {\it one $>$ all, all $>$ one} \\ \hline\hline
SF thumb & thumb position  \\ \cline{2-2}
& {\it N/A, unopposed, opposed} \\ \hline\hline
SF handpart & internal parts of the hand  \\ \cline{2-2}
& {\it SIL, base, palm, ulnar} \\ \hline\hline
UF & open/closed \\ \cline{2-2}
& {\it SIL, open, closed} \\ \hline
\end{tabular}
\caption{Definition and possible values for phonological features based on~\cite{bren}.  The first five features are properties of the active fingers ({\it selected fingers}, SF); the last feature is the state of the inactive or {\it unselected fingers} (UF).  In addition to Brentari's feature values, we add a SIL (``silence'') value to the features that do not have an N/A value.  For detailed descriptions, see~\cite{bren}.}
\label{t:features}
\end{center}
\end{table}
\end{comment}


