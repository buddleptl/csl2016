\subsection{Signer-independent experiments}
\label{sec:sigindep_exp}
In the signer-independent setting, we would like to recognize fignerspelled letter sequences from a new signer, given a model trained on other signers.  For each of the four test signers, we train models on the remaining three signers. However, with such small size of training data (both numbers of signers and data set sizes) this may make the problem very hard, so we only assume small sets of data is available from a test signer with peak annotations or even only letter labels. 

%\begin{table*}[ht!]
\begin{table*}
%\begin{center}
\centering
\resizebox{\linewidth}{!}{
\begin{tabular}{|l||c|c|c|c|c||c|c|c|c|c||c|c|c|c|c|}
\hline
        & \multicolumn{5}{c||}{Tandem HMM} & \multicolumn{5}{c||}{Rescoring SCRF} & \multicolumn{5}{c|}{1st-pass SCRF} \\ \hline
Signer  & 1 & 2 &  3 &  4 & {\bf Mean} &  1 &  2 &  3 &  4 & {\bf Mean} & 1 &  2 &  3 &  4 & {\bf Mean} \\ \hline\hline
No adapt. & 54.1&	54.7&	62.6&	57.5&	{\bf 57.2}&	52.6&	51.2&	61.1&	56.3&	{\bf 55.3}&	55.3&	53.3&	72.5&	61.4&	{\bf 60.6} \\ \hline
%Forced align. & 63.5&	64.4&	51.5&	59.4&	{\bf 59.7}&	65.3&	66.8&	54.2&	60.2&	{\bf 61.6}&	65.8&	68.1&	46.6&	60.2&	{\bf 60.2} \\ \hline
Forced align. & 30.2&    38.5&   39.6&   36.1&   {\bf 33.6}&     39.5&   36.0&   38.2 &   34.5&   {\bf 32.0}&    24.4&   24.9&   36.5&   35.5&   {\bf 30.3} \\ \hline
%Ground truth & 71.3&	81.0&	62.6&	74.2&	{\bf 72.3}&	72.5&	82.2&	66.6&	75.3&	{\bf 74.1}&	77.8&	86.8&	66.9&	77.4&	{\bf 77.2} \\ \hline
Ground truth  & 22.0&    13.0&   31.6&   21.4&   {\bf 22.0}&     22.4&   13.5&   29.5&   21.4&   {\bf 21.7}&     15.2&   10.6&   24.9&   18.4&   {\bf 17.3} \\ \hline
Signer-dependent &     13.7750&    7.0980&   26.0740&   11.5300 & {\bf 14.6}& 10.1960 &   6.9630&   19.1020&    9.9640 &{\bf 11.5} & 8.1&    7.7&    9.3&   10.1&  {\bf 8.8} \\ \hline 
\end{tabular}
\caption{Letter error rate (\%) on four test signers.}
\label{t:LER}
}
\vspace{-.1in}
\end{table*}

The initial unadapted signer-independent DNNs are trained on all but the test signer for each of the seven tasks (letters and the six phonological features). Then we consider DNN normalization and adaptation with different types and amounts of supervision.  For LIN+UP and LIN+LON, we adapt by running SGD over minibatches of $100$ samples with a fixed momentum of $0.9$ for up to $20$ epochs, with initial learning rate of $0.02$ (which is halved when accuracy stops improving on the adaptation data).  For fine-tuning, we use the same SGD procedure as for the signer-independent DNNs.  We pick the epoch with the highest accuracy on the adaptation data.
The resulting frame accuracies are given in Fig.~\ref{fig:adapt}.  In addition, Fig.~\ref{fig:adapt} includes the result of speed normalization for the case of letter classification.  Speed normalization provides consistent but very small improvements, while adaptation gives large improvements in all settings.
LIN+UP slightly outperforms LIN+LON, and fine-tuning outperforms both LIN+UP and LIN+LON. For letter sequence recognition, we adapt via fine-tuning using $20\%$ of the test signer's data.

In Fig.~\ref{fig:confmat}, we further analyze the DNNs via confusion matrices. One of the main effects is the large number of incorrect predictions of the non-signing classes ($<$s$>$, $<$/s$>$).  We observe the same effect with the phonological feature classifiers.  This may be due to the previously mentioned fact that non-linguistic gestures are variable and easy to confuse with signing when given a new signer's image frames.  The confusion matrices show that, as the DNNs are adapted, this is the main type of error that is corrected.




Table~\ref{t:LER} shows the connected letter recognition accuracies obtained with the tandem, rescoring SCRF, and first-pass SCRF models with DNN adaptation via fine-tuning, using different types of adaptation data. For all models, we do not retrain the models with the adapted DNNs, but tune several hyperparameters\footnote{See~\cite{kim2012,kim2013,tang2015} for details of the tuning parameters.} on 10\% of the test signer's data.  The tuned models are evaluated on an unseen 10\% of the test signer's remaining data; finally, we repeat this for eight choices of tuning and test sets, covering the 80\% of the test signer's data that we do not use for adaptation, and report the mean letter accuracy over the test sets.  

As shown in Table~\ref{t:LER}, without adaptation both tandem and SCRF models do poorly, achieving only roughly 40\% letter accuracies, with the rescoring SCRF slightly outperforming the others.  With adaptation, however, performance jumps to up to 69.7\% letter accuracy with forced-alignment adaptation labels and up to 82.7\% accuracy with ground-truth adaptation labels.  All of the adapted models perform similarly, but interestingly, the first-pass SCRF is slightly worse than the others before adaptation and better (by 4.4\% absolute) after ground-truth adaptation.  One hypothesis is that the first-pass SCRF is more dependent on the DNN performance, while the tandem model uses the original image features and the rescoring SCRF uses the tandem model hypotheses and scores.  Once the DNNs are adapted, however, the first-pass SCRF outperforms the other models.

\begin{comment}


\begin{figure}\centering
\centering
\vspace{-.06in}
\hspace{-.2in}\includegraphics[width=0.92\linewidth]{figs/adapt_bar3b.eps}
\vspace{-.15in}
\caption{DNN frame accuracies with and without normalization/adaptation.  The horizontal axis labels indicate the amount of adaptation data (0, 1, 2, 3 = none, 5\%, 10\%, 20\% of the test signer's data, corresponding to no adaptation, $\sim29$, $\sim58$, and $\sim115$ words).  GT = ground truth labels; FA = forced alignment labels; FT = fine-tuning.}
\label{fig:adapt}
\end{figure}



\begin{figure*}[t]
\centering
\begin{tabular}{@{}c@{\hspace{0.02\linewidth}}c@{\hspace{0.01\linewidth}}c@{\hspace{0.01\linewidth}}c@{}}
%& No Adapt. $C1$ & \caja{c}{c}{LIN+UP\\Forced-Align.} $C2$ & \caja{c}{c}{LIN+UP\\Ground Truth} $C3$ \\
No Adapt. & \caja{c}{c}{LIN+UP (Forced-Align.)} & \caja{c}{c}{LIN+UP (Ground Truth)} \\
%\rotatebox{90}{\hspace*{0em}Letter} &
\includegraphics[width=0.34\linewidth]{confusion_noadaptation_andy_task=letter.eps} & 
\includegraphics[width=0.28\linewidth]{confusion_forced_aligned_adaptation_andy_task=letter.eps} & 
\includegraphics[width=0.34\linewidth]{confusion_ground_truth_adaptation_andy_task=letter.eps} \\
\end{tabular}
\vspace{-.1in}
\caption{Confusion matrices of DNN classifiers for one test signer (Signer 1).  $20\%$ of the test signer's data (115 words) was used for adaptation, and a disjoint $70\%$ was used to compute confusion matrices.  
%POR refers to the ``point of reference'' phonological feature~\cite{kim2012}.  
Each matrix cell is the empirical probability of the predicted class (column) given the ground-truth class (row).  The diagonal has been zeroed out for clarity.}
\label{fig:confmat}
\end{figure*}

\end{comment} 





