\section{Introduction}
\label{sec:intro}

Sign languages are the primary means of communication for millions of Deaf people in the world.  
In the US, there are about 350,000--500,000 people for whom American Sign Language (ASL) is the primary language~\cite{Mitchell:2006}.  
While there has been extensive research over several decades on automatic recognition and analysis of spoken language, much less progress has been made for sign languages.
Both signers and non-signers would benefit from technology that improves communication between these populations and facilitates search and retrieval in sign language video.

Sign language recognition involves major challenges.  The linguistics of sign language is less well understood than that of spoken language, hampering both scientific and technological progress.
Another challenge is the high variability in the appearance of signers' bodies and the large number of degrees of freedom. 
The closely related computer vision problem of articulated pose estimation and tracking remains largely unsolved.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{fingerspelling.pdf}
\caption{The ASL fingerspelled alphabet.  Reproduced from~\cite{Keane2014diss}.}
\label{fig:alphabet}
\end{figure}

Signing in ASL (as in many other sign languages) involves the simultaneous use of handshape, location, movement, orientation, and non-manual behaviors.  There has now been a significant amount of research on sign language recognition, for a number of sign languages.  Prior research has focused more on the larger motions of sign and on interactions between multiple body parts, and less so on handshape.  In some contexts, however, handshape carries much of the content, and it is this aspect of ASL that we study here.

Sign language handshape has its own phonology that has been studied and enjoys a broadly agreed-upon understanding relative to the other manual parameters of movement and place of articulation~\cite{bren}.
%but does not yet enjoy a broadly agreed-upon understanding
Recent linguistic work on sign language phonology has developed approaches based on articulatory features, related to motions of parts of the hand \cite{DingM09,Keane2014diss}.
At the same time, computer vision research has studied pose estimation and tracking of hands \cite{tang2013real}, but usually not in the context of a grammar that constrains the motion.  
There is therefore a great need to better understand and model the handshape properties of sign language.

This project focuses mainly on one constrained,
%\htcomment{name conflict with unconstrained fingerspelling?}
but very practical, component of ASL:  fingerspelling.  In certain contexts
(e.g., when no sign exists for a word such as a name, to introduce a
new word, or for emphasis), signers use fingerspelling: They spell out
the word as a sequence of handshapes or hand trajectories
corresponding to individual letters.  Fig.~\ref{fig:alphabet} shows
the ASL fingerspelled alphabet, and Figs.~\ref{fig:ex} show
examples of real fingerspelling sequences.  We will refer to the handshapes in Fig.~1 as fingerspelled letters (FS-letters), which are canonical target handshapes for each of the 26 letters, and the starred actual handshapes in Fig.~2 as peak handshapes.
%(peak-HSs).
%\kl{I moved the example figs to this section}

Fingerspelled words arise naturally in the context of technical conversation or conversation about current events, such as in Deaf blogs or news sites.\footnote{E.g., \tt{http://deafvideo.tv, http://aslized.org}.}  Automatic fingerspelling recognition could add significant value to such resources.
Overall, fingerspelling comprises 12-35\% of ASL~\cite{Padden:2003}, depending on the context, and includes 72\% of the handshapes used in ASL~\cite{Brentari:2001}.  
These factors make fingerspelling an excellent testbed for handshape recognition.

%\begin{comment}
\begin{figure}[t]
\includegraphics[width=\linewidth]{ex_tulip}
\includegraphics[width=\linewidth]{ex_art}
\caption{Images and ground-truth segmentations of the fingerspelled
  words \fs{TULIP} and \fs{ART} produced by two signers in our data set. Image frames are sub-sampled
  at the same rate from both signers to show the true relative speeds.
  The starred frames indicate manually annotated peak handshapes (peak-HSs) for each FS-letter (see the text for the full definition of FS-letter and peak-HS).
  ``\texttt{<s>}'' and ``\texttt{</s>}'' denote non-signing intervals
  before/after signing.  See Sec.~\ref{sec:data},~\ref{sec:method} for more
  details on data collection, annotation, and segmentation.}
\label{fig:ex}
\end{figure}
%\end{comment}

Most previous work on fingerspelling and handshape has focused on
restricted conditions such as careful articulation, isolated signs,
restricted (20-100 word) vocabularies, or signer-dependent
applications~\cite{goh,Liwicki-Everingham-09,Ricco-Tomasi-09} (see
Section~\ref{sec:related_work} for more on related work).  In such
restricted settings, letter error rates (Levenshtein distances between
hypothesized and true FS-letter sequences, as a proportion of the number
of true FS-letters) of 10\% or less have been obtained.  In this work we
consider {\it lexicon-free} fingerspelling
%\htcomment{name conflict? see unrestricted fingerspelling}
sequences produced by
multiple signers.  This is a natural setting, since fingerspelling is
often used for names and other ``new'' terms that do not appear in any
closed vocabulary.  Our long-term goals are to develop techniques for
robust automatic detection and recognition of fingerspelled words in
video, and to enable generalization across signers, styles, and
recording conditions, both in controlled visual settings and ``in the wild.''  To this end, we are also interested in developing multi-signer, multi-style corpora of fingerspelling, as well as efficient annotation schemes for the new data.  

The work in this paper represents our first steps toward this goal:
studio collection and annotation of a new multi-signer connected
fingerspelling data set (Section~\ref{sec:data}) and high-quality
fingerspelling recognition in the signer-dependent and
multi-signer settings (Section~\ref{sec:exp}).  The new data
set, while small relative to typical speech data sets, comprises the
largest fingerspelling video data set of which we are aware containing
connected multi-signer fingerspelling that is not restricted to any particular lexicon.  Next, we compare several
recognition models inspired by automatic speech recognition, with some
custom-made characteristics for ASL
(Section~\ref{sec:method}).  We begin with a tandem hidden Markov
model (HMM) approach, where the features are based on posteriors of
deep neural network (DNN) classifiers of letter and handshape phonological
features.  We also develop discriminative segmental models, which
allow us to introduce more flexible features of fingerspelling
segments.  In the category of segmental models, we compare models for
rescoring lattices and a first-pass segmental model, and the latter
ultimately outperforms the others.  Finally, we address the problem
of signer variation via adaptation of the DNN classifiers, which
allows us to bridge much of the gap between signer-dependent and
signer-independent performance. \footnote{Parts of this
  work have appeared in our conference
  papers~\cite{kim2012,kim2013,kim2016}.  This paper includes additional model comparisons and improvements, different linguistic feature
  sets, and detailed presentation of the collected data and annotation.}



%\input{ex_tulip}
%\input{ex_art}
%\begin{figure}[h!]
%\begin{center}
%\includegraphics[width=0.8\linewidth]{figs/ex_way}
%\caption{Images and ground-truth segmentations of the fingerspelled
%  word \fs{WAY} produced by two signers. \kl{this doesn't look like
%    ``way'' at all (looks more like ``art'') -- double-check.  also
%    need to make the formatting match between the two example figs,
%    e.g. the time bar and the font}}
%\end{center}
%\label{fig:ex2}
%\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{comment}
In SCRFs~\cite{sarawagisemi,zweig}, feature functions are defined over
segments of observed variables (in our case, any number of consecutive
video frames) and their corresponding labels (in our case, letters).
The use of such segmental feature functions is useful for gesture modeling, where it is natural to consider the trajectory of some measurement or the statistics of an entire segment.
In this work we define feature functions based on scores of letter classifiers, as well as classifiers of {\it handshape features} suggested by linguistics research on ASL~\cite{bren,kim2012}.  Linguistic handshape features summarize certain important aspects of a given letter, such as the ``active'' fingers or the flexed/non-flexed status of the fingers; these are hopefully the most salient aspects of each letter, which should be more discriminative for recognition.
We also use the statistics of raw visual descriptor derivatives over entire segments to define basic ``expected motion'' feature functions.

Recognition of fingerspelled letter sequence from a new unseen signer
presents challenges due to significant variations among signers,
coupled with the dearth of available training data.  
Prior work has addressed signer adaptation for large-vocabulary German
Sign Language recognition~\cite{forster2013improving}, but to our
knowledge this paper is the first to address adaptation specifically for
fingerspelling or handshape.  We 
investigate approaches to signer-independence including speed normalization and neural network adaptation.  The adaptation techniques are largely borrowed from speech recognition research, but the application is quite different in that the overall amount of data is much smaller and the types of variation are different.  We find that the simple signer normalization is ineffective, while DNN adaptation is very effective.  
\end{comment}

