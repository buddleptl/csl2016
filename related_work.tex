\section{Related work}
\label{sec:related_work}

Automatic sign language recognition has been approached in a variety of ways, including approaches based on computer vision techniques and ones inspired by automatic speech recognition.  Thorough surveys on the topic are provided by Koller {\it et al.}~\cite{koller2015continuous} and Ong and Ranganath~\cite{ong2005automatic}.  Here we focus on the most closely related work.

A great deal of effort has been aimed at exploiting specialized equipment such as depth sensors, motion capture, data gloves, or colored gloves in video~\cite{Oz-Leu-05,Wang-Popovic-09,pugeault2011spelling,keskin2012hand,lu2014collecting,zhang2015histogram,dong2015}.  This approach is very attractive for designing new communication interfaces for Deaf individuals.  In many settings, however, video is more practical, and for online or archival recordings is the only choice.  In this paper we restrict the discussion to the video-only setting.

A number of sign language video corpora have been collected \cite{Martinez-et-al-02, Dreuw-et-al-08, von2008significance, Dreuw-et-al-2010, Athitsos-et-al-10,neidle2012new}.  Some of the largest data collection efforts have been for European languages, such as the Dicta-Sign~\cite{efthimiou2012sign} and SignSpeak~\cite{forster2012rwth} projects.
For American Sign Language, the American Sign Language Lexicon Video Dataset (ASLLVD)~\cite{Athitsos-et-al-10,neidle2012new,ASLLVD} includes recordings of almost 3,000 isolated signs and can be searched via a query-by-example interface.  The National Center for Sign Language and Gesture Resources (NCSLGR) Corpus includes videos of continuous ASL signing, with over 10,000 sign tokens including about 1,500 fingerspelling sequences, annotated using the SignStream linguistic annotation tool~\cite{NCSLGR,Neidle_Sclaroff_Athitsos_2001}.  The latter includes the largest previous data set of fingerspelling sequences of which we are aware.  In order to explicitly study fingerspelling, however, it is helpful to have a collection that is both larger and annotated specifically with fingerspelling in mind.  To our knowledge, the data collection and annotation we report in this paper (see Sec.~\ref{sec:data}) is the largest currently available for ASL fingerspelling.

Sign language recognition from video begins with front-end features.  Prior work has used a variety of visual 
features, including ones based on estimated position, shape and movement of the hands and
head~\cite{Bowden-et-al-04,Yang-Sarkar-06,Zahedi-et-al-06,Zaki-Shaheen-11}, sometimes combined with appearance
descriptors~\cite{Farhadi-et-al-07,Dreuw-et-al-07,Nayak-et-al-09,DingM09}
and color models~\cite{Buehler11,Pfister12}.  In this work, we are
aiming at relatively small motions that are difficult to track a
priori, and therefore begin with general image appearance features
based on histograms of oriented gradients (HOG)~\cite{Dalal-Triggs-05}, which have also been used in other recent sign language recognition work~\cite{koller2015continuous,jangyodsuksign}.

Much prior work has used hidden Markov model (HMM)-based approaches~\cite{Starner-Pentland-98,Vogler-Metaxas-99,Dreuw-et-al-07,koller2015continuous}, and this is the starting point for our work as well.  Ney and colleagues have shown that it is possible to borrow many of the standard HMM-based techniques from automatic speech recognition to obtain good performance on naturalistic German Sign Language videos~\cite{Dreuw-et-al-07,koller2015continuous}.
In addition, there have been efforts involving conditional
models~\cite{Yang-Sarkar-06} and more complex (non-linear-chain)
models~\cite{ThaEtal2011,VoglerM03,theodorakis2009product}.  

As in acoustic and even visual speech recognition, the choice of basic linguistic unit is an important research question.  For acoustic speech, the most commonly used unit is the context-dependent phoneme~\cite{odell:thesis}, although other choices such as syllables, articulatory features, and automatically learned units have also been considered~\cite{Ost99,livescu2012subword}.  As the research community started to consider visual speech recognition (``lipreading''), analogous units have been explored:  visemes, articulatory features, and automatic clusters~\cite{potamianos2015audio,hazen2004segment,saenko2009multistream}.
While sign language shares some aspects of spoken language, it has some unique characteristics.  A number of linguistically motivated representations of handshape and motion have been used in some prior work, e.g.,~\cite{Bowden-et-al-04,DingM09,ThaEtal2011,VoglerM03,VoglerM01,VoglerM99,Vogler-Metaxas-99,Pit2011,The2010}, and multiple systems of phonological and phonetic features have been developed by linguists~\cite{Brentari:1998,Keane2014diss}.  One of the unique aspects of sign language is that transitional movements occupy a larger portion of the signal than steady states, and some researchers have developed approaches for explicitly modeling the transitions as units~\cite{VoglerM97,Yang2010,The2011,li2016sign}.

A subset of ASL recognition work has focused specifically on fingerspelling and/or handshape classification~\cite{Athitsos-et-al-04,Rousos-et-al-10,ThaEtal2011,pugeault2011spelling} and fingerspelling sequence recognition~\cite{goh,
%Bowden-Sarhadi-02,Tsechpenakis-et-al-06-coupling,
Liwicki-Everingham-09,Ricco-Tomasi-09}.  Letter error rates of 10\% or less have been achieved when the recognition is constrained to a small (up to 100-word) lexicon of allowed sequences.
Our work is the first of which we are aware to address the task of {\it lexicon-free} fingerspelling
sequence recognition.

The problem of signer adaptation has been addressed in prior work, using techniques borrowed from speaker adaptation for speech recognition, such as maximum likelihood linear regression and maximum a posteriori estimation~\cite{koller2015continuous,von2008rapid,ong2004deciphering}, and to explicitly study the contrast between signer-dependent and multi-signer fingerspelling recognition.

The models we use and develop in this paper are related to prior work in both vision and speech (unrelated to sign language).  Our HMM baselines are tandem models, analogous to ones developed for speech recognition~\cite{Her00,grezl2007probabilistic}.  The segmental models we propose are related to segmental conditional random fields (SCRFs) and their variants, which have now been applied fairly widely for speech recognition~\cite{zweig,tang2015,lu2016segmental,zweig2011speech,he2012efficient}.
In natural language processing, semi-Markov CRFs have been used for named entity recognition~\cite{sarawagisemi}, where the labeling is binary.
Finally, segmental models have been applied to vision
tasks such as classification and segmentation of action sequences~\cite{shi2011human,duong2005activity} with a small set of possible activities to choose from, including work on spotting and recognition of a small vocabulary of (non-fingerspelled) signs in sign language video~\cite{Choetal2009} or instrumented data capture~\cite{kong2014towards}.
One aspect that our work shares with the speech recognition work is that we have a relatively large set of labels (26 FS-letters plus non-letter ``N/A'' labels), and widely varying lengths of segments corresponding to each label (our data includes segment durations anywhere from 2 to 40 frames), which makes the search space larger and the recognition task more difficult than in the vision and text tasks.
In prior speech recognition work, this computational difficulty has
often been addressed by adopting a lattice rescoring approach, where a
frame-based model such as an HMM system generates first-pass lattices
and a segmental model rescores them~\cite{zweig,zweig2011speech}.  We
compare this approach to an efficient first-pass segmental model~\cite{tang2015}.




