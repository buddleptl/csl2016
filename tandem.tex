\subsection{A Tandem Model}
We base our approach to fingerspelling recognition on the popular tandem approach to speech recognition~\cite{ellis}.  In tandem-based speech recognition, multilayer perceptrons (MLPs) are trained to classify phones, and their outputs (phone posteriors) are post-processed and used as observations in a standard HMM-based recognizer with Gaussian mixture observation distributions.  The post-processing may include taking the logs of the posteriors (or simply taking the linear outputs of the MLPs rather than posteriors), applying principal components analysis, and/or appending acoustic features to the MLP outputs.

In this work, we begin with a basic adaptation of the tandem approach, where instead of phone posteriors estimated from acoustic frames, we use letter posteriors estimated from image features (described in Section~\ref{sec:experiments}).  Next, we propose a tandem model using classifiers of phonological features of fingerspelling rather than of the letters themselves.  The motivation is that, since features have fewer values, it may be possible to learn them more robustly than letters from small training sets, and certain features may be more or less difficult to classify.  This is similar to the work in speech recognition of \c{C}etin et al.~\cite{cetin}, who used articulatory feature MLP classifiers rather than phone classifiers.

We use a phonological feature set developed by Brentari~\cite{bren}, who proposed seven features for ASL handshape.  Of these, we use the six that are contrastive in fingerspelling.  The features and their values are given in Table~\ref{t:features}.  
 Example frames for values of the ``SF thumb'' feature are shown in Figure~\ref{f:feature_ex}, and entire phonological feature vectors for several letters are shown in Figure~\ref{f:example}. 
We also show examples for one of those features, thumb, in Figure~\ref{f:feature_ex}. 
The observations used in our HMMs consist of functions of the MLP posteriors for all values of all MLPs, concatenated with image appearance features.  In the feature-based tandem case, there is a total of 26 posteriors per frame; in the letter-based tandem case, there are 28 (26 letters + beginning ``silence'' + ending ``silence'').

\begin{table}[t]
\begin{center}
\begin{tabular}{|l|l|}
\hline
{\bf Feature} & {\bf Definition/Values}  \\ \hline \hline
SF point of & side of the hand where \\ 
reference & SFs are located  \\ \cline{2-2} 
(POR) & {\it SIL, radial, ulnar, radial/ulnar} \\ \hline\hline
SF joints & degree of flexion or \\
& extension of SFs  \\  \cline{2-2}
& {\it SIL, flexed:base, flexed:nonbase,} \\ & {\it flexed:base \& nonbase,} \\
& {\it stacked, crossed, spread} \\ \hline\hline
SF quantity &combinations of SFs \\ \cline{2-2}
& {\it N/A, all, one,} \\ & {\it one $>$ all, all $>$ one} \\ \hline\hline
SF thumb & thumb position  \\ \cline{2-2}
& {\it N/A, unopposed, opposed} \\ \hline\hline
SF handpart & internal parts of the hand  \\ \cline{2-2}
& {\it SIL, base, palm, ulnar} \\ \hline\hline
UF & open/closed \\ \cline{2-2}
& {\it SIL, open, closed} \\ \hline
\end{tabular}
\caption{Definitions and values of phonological features based on~\cite{bren}.  The first five features refer to the active fingers ({\it selected fingers}, SF); the last is the state of the inactive or {\it unselected} fingers (UF).  In addition to Brentari's feature values, we add a SIL (``silence'') value to features that lack an N/A value.  For more details, see~\cite{bren}.}
\label{t:features}
\end{center}
\end{table}
