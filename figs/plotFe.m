load stat02222016diffLingFe.mat

figure;
DD = [DDa, mean(DDa,2)];
hold on
h=bar(100-DD');
set(h(1),'facecolor','r');set(h(2),'facecolor','y');set(h(3),'facecolor','g');set(h(4),'facecolor','b');
legend(h,'letter only','letter+phonology','phonetic only','letter+phonetic');legend boxoff;
axis([0.5 5.5 0 30]);  ylabel('Letter Error Rate (%)');
ax = gca;
ax.XTickLabel = {'','1','','2','','3','','4','','mean',''};
xlabel('signers');
hold off
print -depsc ling_fe_indep_adapt.eps

figure;
DD = [DDua, mean(DDua,2)];
hold on
h=bar(100-DD');
set(h(1),'facecolor','r');set(h(2),'facecolor','y');set(h(3),'facecolor','g');set(h(4),'facecolor','b');
legend(h,'letter only','letter+phonology','phonetic only','letter+phonetic');legend boxoff;
axis([0.5 5.5 40 75]);  ylabel('Letter Error Rate (%)');
ax = gca;
ax.XTickLabel = {'','1','','2','','3','','4','','mean',''};
xlabel('signers');
hold off
print -depsc ling_fe_indep_unadapt.eps


figure;
DD = [DDdep, mean(DDdep,2)];
hold on
h=bar(100-DD');
set(h(1),'facecolor','r');set(h(2),'facecolor','y');set(h(3),'facecolor','g');set(h(4),'facecolor','b');
legend(h,'letter only','letter+phonology','phonetic only','letter+phonetic');legend boxoff;
axis([0.5 5.5 0 16]);  ylabel('Letter Error Rate (%)');
ax = gca;
ax.XTickLabel = {'','1','','2','','3','','4','','mean',''};
xlabel('signers');
hold off
print -depsc ling_fe_dep.eps












