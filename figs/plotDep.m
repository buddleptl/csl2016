load stat02222016diffLingFe.mat DDdep


Dmlp = [ 90.108,0.78193,	93.3922,1.4002
90.642,1.0718,	93.8827,1.1292
87.551,1.5545,	93.4402,1.3935
88.596,0.68082,	91.962,1.5358]; % mlp acc.

Ddnn = [0.0800    0.0689    0.0995    0.0955
    0.0517    0.0456    0.0514    0.0644];
Ddnn = 100-Ddnn*100;


Dold = [85.966	86.225	89.804
91.611	92.902	93.037
71.384	73.926	80.898
85.742	88.47	90.036]; % old LER (iccv2013)


figure;
clear DDa
DDa = [Dold';DDdep(2,:)];
DD = [DDa, mean(DDa,2)];
hold on
h=bar(100-DD');
set(h(1),'facecolor','r');set(h(2),'facecolor','y');set(h(3),'facecolor','g');set(h(4),'facecolor','b');
legend(h,'Baseline HMM','Tandem HMM','SCRF rescoring','1st-pass SCRF');legend boxoff;
axis([0.5 5.5 0 30]);  ylabel('Letter Error Rate (%)');
ax = gca;
ax.XTickLabel = {'','1','','2','','3','','4','','mean',''};
xlabel('signers');
hold off
print -depsc LER_dep.eps




figure;
clear DDa
DDa = [(Dmlp(:,[1,3]))';Ddnn];
DDa = [DDa(1,:);DDa(3,:);DDa(2,:);DDa(4,:)];
DD = [DDa, mean(DDa,2)];
hold on
h=bar(100-DD');
set(h(1),'facecolor','r');set(h(2),'facecolor','y');set(h(3),'facecolor','g');set(h(4),'facecolor','b');
legend(h,'letter MLP','letter DNN','feature MLP','feature DNN');legend boxoff;
axis([0.5 5.5 0 15]);  ylabel('Frame Error Rate (%)');
ax = gca;
ax.XTickLabel = {'','1','','2','','3','','4','','mean',''};
xlabel('signers');
hold off
print -depsc FRacc_dep.eps



