\section{Experimental Results}
\label{sec:exp}

%\kl{removed figs for now, waiting for new figs from Taehwan according to the new structure we discussed.}


We report on experiments using the fingerspelling data from the four
ASL signers described above.  We begin by describing some of the
front-end details of hand segmentation and feature extraction,
followed by experiments with the frame-level DNN classifiers
(Sec.~\ref{sec:dnn-exp}) and FS-letter sequence recognizers (Sec.~\ref{sec:LER-exp}).\\

\noindent{\bf Hand localization and segmentation}
As in prior work~\cite{Liwicki-Everingham-09,kim2012,kim2013},
we used a simple signer-dependent model for hand detection.  
First we manually annotated hand regions in 30 frames, and we trained a mixture of Gaussians $P_{hand}$ for the color of the hand pixels in L*a*b color space, and a single-Gaussian color model $P_{bg}^x$ for every pixel $x$ in the image excluding pixel values in or near marked hand regions. 
Given the color triplet $\mathbf{c}_x=[l_x,a_x,b_x]$ at pixel $x$ from
a test frame, we assign the pixel to the hand if
\begin{equation}
  \label{eq:handodds}
  P_{hand}(\mathbf{c}_x)\pi_{hand}\,>\,
  P_{bg}^x(\mathbf{c}_x)(1-\pi_{hand}),
\end{equation}
where the prior $\pi_{hand}$ for hand size is estimated from the same 30
training frames.

We clean up the output of this simple model via several filtering
steps.  First, we suppress pixels that fall
within regions detected as faces by the Viola-Jones face detector~\cite{violajones},
since these tend to be false positives.  We also suppress pixels that
passed the log-odds test (Eq.~\ref{eq:handodds}) but have a low estimated value of $P_{hand}$, since
these tend to correspond to movements in the scene.  Finally, we suppress pixels outside of a
(generous) spatial region where the signing is expected to occur. The largest surviving connected
component of the resulting binary map is treated as a mask that
defines the detected hand region. Some examples of the resulting hand
regions are shown in
Fig.~\ref{fig:ex}. Although this approach currently involves
manual annotation for a small number of frames, it could be fully
automated in an interactive system, and may not be required if given a
larger number of training signers. \\ 
%\kl{Greg/Taehwan, please check if
%  you agree with this statement}\\


\noindent{\bf Handshape descriptors} We use histograms of oriented
gradients (HOG~\cite{Dalal-Triggs-05}) as the visual descriptor
(feature vector) for a given hand region.
We first resize the tight bounding box of the hand region to a
canonical size of 128$\times$128 pixels, and then compute HOG features
on a spatial pyramid of regions, 4$\times$4,
8$\times$8, and 16$\times$16 grids,  with eight orientation bins per
grid cell, resulting in 2688-dimensional descriptors.  Pixels outside of the
hand mask are ignored in this computation. For the HMM-based recognizers, to speed
up computation, these descriptors were projected to at most 200
principal dimensions; the 
exact dimensionality in each experiment was tuned on a development set. For DNN frame classifiers, we found that finer grids did not improve much with increasing complexities, so we use 128-dimensional descriptors.\\

\subsection{DNN frame classification performance}
\label{sec:dnn-exp}
\begin{figure}\centering
\centering
\vspace{-.06in}
\hspace{-.0in}\includegraphics[width=0.6\linewidth]{adapt_fer_le} \\
\hspace{-.0in}\includegraphics[width=0.6\linewidth]{adapt_fer_ph_fe}
%\hspace{-.0in}\includegraphics[width=1\linewidth]{figs/adapt_fer_le.eps} \\
%\hspace{-.0in}\includegraphics[width=1\linewidth]{figs/adapt_fer_ph_fe.eps}
\vspace{-.15in}
\caption[Frame errors with DNN classifiers, with various settings]{Frame errors with DNN classifiers, with various settings. The horizontal axis labels indicate the amount of adaptation data (0, 1, 2, 3 = none, 5\%, 10\%, 20\% of the test signer's data, corresponding to no adaptation (signer-independent), $\sim29$, $\sim58$, and $\sim115$ words).  GT = ground truth labels; FA = forced alignment labels; FT = fine-tuning. We also added trained DNN on only 20\% of the test signer's data. Signer-dependent DNN uses 80\% of the test signer's data.}
\label{fig:adapt_fer}
\end{figure}

%\kl{where are training/dev/test splits described for this section?}
Since all of our fingerspelling recognition models use DNN frame classifiers as a building block,
we first examine the performance of the frame classifiers.

For training and tuning the DNNs, we use the recognizer training data, split into 90\% for DNN training and 10\% for DNN tuning.
The DNNs are trained for seven tasks (FS-letter classification and
classification of each of the six phonological features). The input is
the $128$-dimensional HOG features concatenated over a $21$-frame window.
The DNNs have
 three hidden layers, each with $3000$ ReLUs~\cite{Zeiler_13a}.
 Network learning is done with cross-entropy training with a weight
 decay penalty of $10^{-5}$, via stochastic gradient descent (SGD)
 over $100$-sample minibatches for up to $30$ epochs, with
 dropout~\cite{Srivas_14a} at a rate of $0.5$ at each hidden layer,
 fixed momentum of $0.95$, and initial learning rate of $0.01$, which
 is halved when held-out accuracy stops improving.  We pick the best-performing epoch on held-out data.  The network
 structure and hyperparameters were tuned on held-out (signer-independent) data in initial experiments.  

We consider the signer-dependent setting (where the DNN is trained on
data from the test signer), signer-independent setting (where the DNN
is trained on data from all except the test signer), and
signer-adapted setting (where the signer-independent DNNs are adapted
using adaptation data from the test signer).
 For LIN+UP and LIN+LON, we adapt by running SGD over minibatches of $100$ samples with a fixed momentum of $0.9$ for up to $20$ epochs, with initial learning rate of $0.02$ (which is halved when accuracy stops improving on the adaptation data).  For fine-tuning, we use the same SGD procedure as for the signer-independent DNNs.  We pick the epoch with the highest accuracy on the adaptation data.

The frame error rates for all settings are given in
Fig.~\ref{fig:adapt_fer}.  For the signer-adapted case, we consider DNN
adaptation with different types and amounts of supervision.  The types
of supervision include fully labeled adaptation data (``GT'', for
``ground truth'', in the figure), where the peak locations for all
FS-letters are manually annotated; as well as adaptation data labeled
only with the FS-letter sequence but not the timing information.  In the
latter case, we use the baseline tandem system to generate forced
alignments (``FA'' in the figure).  We consider amounts of adaptation
data from 5\% to 20\% of the test signer's full data.

These results show that among the adaptation methods, LIN+UP slightly outperforms LIN+LON, and fine-tuning outperforms both
LIN+UP and LIN+LON.  For FS-letter sequence recognition experiments in
the next section, we adapt via fine-tuning using $20\%$ of the test signer's data.

We have analyzed the types of errors made by the DNN classifiers.  One
of the main effects is that all of the signer-independent classifiers
have a large number of incorrect predictions of the non-signing
classes ($<$s$>$, $<$/s$>$).  
%We observe the same effect with the phonological feature classifiers.  
This may be due to the previously mentioned observation that
non-linguistic gestures are variable and easy to confuse with signing
when given a new signer's image frames.  
%The confusion matrices show that, a
As the DNNs are adapted, this is the main type of error that is corrected. %\kl{would be great to quantify this a bit}
Specifically, of the frames with label $<$s$>$ that are misrecognized by the signer-independent DNNs, 18.3\% are corrected after adaptation; of the misrecognized frames labeled $<$/s$>$, 11.2\% are corrected.

% via confusion matrices. One of the main effects is the large number of incorrect predictions of the non-signing classes ($<$s$>$, $<$/s$>$).  We observe the same effect with the phonological feature classifiers.  This may be due to the previously mentioned fact that non-linguistic gestures are variable and easy to confuse with signing when given a new signer's image frames.  The confusion matrices show that, as the DNNs are adapted, this is the main type of error that is corrected.

\subsection{FS-letter recognition experiments}
\label{sec:LER-exp}

\subsubsection{Signer-dependent recognition}
Our first continuous FS-letter recognition experiments are signer-dependent; that is, we train and test on the same signer, for each of four signers. 
%\noindent{\bf Experimental setup}  
For each signer, we use a 10-fold setup:  In each fold, 80\% of the data is used as a training set, 10\% as a development set for tuning parameters, and the remaining 10\% as a final test set.    
We independently tune the parameters in each fold.  To make the results comparable to the later adaptation results, we use 8 out of 10 folds to compute the final test results and report the average letter error rate (LER) over those 8 folds.
For language models, we train letter bigram language models from large online dictionaries of varying sizes that include both English words and names~\cite{csr}. 
We use HTK~\cite{htk} to implement the baseline HMM-based recognizers and SRILM~\cite{srilm} to train the language models. 
The HMM parameters (number of Gaussians per state, size of language model vocabulary, transition penalty and language model weight), as well as the dimensionality of the HOG descriptor input and HOG depth, were tuned to minimize development set letter error rates for the baseline HMM system.  The front-end and language model hyperparameters were then kept fixed for the experiments with SCRFs (in this sense the SCRFs are slightly disadvantaged).  %The NN output type was tuned separately for the tandem HMM and the SCRF. 
Additional parameters tuned for the SCRF rescoring models included the N-best list sizes, type of feature functions, choice of language models, and L1 and L2 regularization parameters.  Finally, for the first-pass SCRF, we tuned step size, maximum length of segmentations, and number of training epochs. 
%For DNN structure, we use three hidden layers and 3000 ReLUs~\cite{Zeiler_13a} hidden units for each hidden layer. Inputs to DNNs are concatenated 21 frames of HoG feature.

Tab.~\ref{t:LER} (last row) shows the signer-dependent FS-letter recognition results. SCRF rescoring improves over the tandem HMM, and the first-pass SCRF outperforms both.

Note that in our experimental setup, there is some overlap of word
types between training and test data.  This is a realistic setup,
since in real applications some of the test words will have been
previously seen and some will be new.  However, for comparison, we
have also conducted the same experiments while keeping the training,
development, and test vocabularies disjoint; in this modified setup,
letter error rates increase by about 2-3\% overall, but the SCRFs
still outperform the other models.

\begin{table*}[ht]
%\begin{center}
\centering
\resizebox{\linewidth}{!}{
\begin{tabular}{|l||r|r|r|r|r||r|r|r|r|r||r|r|r|r|r|}
\hline
        & \multicolumn{5}{c||}{Tandem HMM} & \multicolumn{5}{c||}{Rescoring SCRF} & \multicolumn{5}{c|}{First-pass SCRF} \\ \hline
Signer  & 1 & 2 &  3 &  4 & {\bf Mean} &  1 &  2 &  3 &  4 & {\bf Mean} & 1 &  2 &  3 &  4 & {\bf Mean} \\ \hline\hline
No adapt. & 54.1&	54.7&	62.6&	57.5&	{\bf 57.2}&	52.6&	51.2&	61.1&	56.3&	{\bf 55.3}&	55.3&	53.3&	72.5&	61.4&	{\bf 60.6} \\ \hline
%Forced align. & 63.5&	64.4&	51.5&	59.4&	{\bf 59.7}&	65.3&	66.8&	54.2&	60.2&	{\bf 61.6}&	65.8&	68.1&	46.6&	60.2&	{\bf 60.2} \\ \hline
Forced align. & 30.2&    38.5&   39.6&   36.1&   {\bf 33.6}&     39.5&   36.0&   38.2 &   34.5&   {\bf 32.0}&    24.4&   24.9&   36.5&   35.5&   {\bf 30.3} \\ \hline
%Ground truth & 71.3&	81.0&	62.6&	74.2&	{\bf 72.3}&	72.5&	82.2&	66.6&	75.3&	{\bf 74.1}&	77.8&	86.8&	66.9&	77.4&	{\bf 77.2} \\ \hline
Ground truth  & 22.0&    13.0&   31.6&   21.4&   {\bf 22.0}&     22.4&   13.5&   29.5&   21.4&   {\bf 21.7}&     15.2&   10.6&   24.9&   18.4&   {\bf 17.3} \\ \hline
Signer-dep. &     13.8&    7.1&   26.1&   11.5 & {\bf 14.6}& 10.2 &   7.0&   19.1&    10.0 &{\bf 11.5} & 8.1&    7.7&    9.3&   10.1&  {\bf 8.8} \\ \hline 
%Signer-dep. &     13.7750&    7.0980&   26.0740&   11.5300 & {\bf 14.6}& 10.1960 &   6.9630&   19.1020&    9.9640 &{\bf 11.5} & 8.1&    7.7&    9.3&   10.1&  {\bf 8.8} \\ \hline 
\end{tabular}
}
\vspace{-.1in}
\caption{Letter error rates (\%) on four test signers.}
\label{t:LER}
%\end{center}
\end{table*}

\subsubsection{Signer-independent recognition}
\label{sec:sigindep_exp}

In the signer-independent setting, we would like to recognize
FS-letter sequences from a new signer, given a model
trained only on data from other signers.  For each of the four test
signers, we train models on the remaining three signers, and report
the performance for each test signer and averaged over the four test
signers.  For direct comparison with the signer-dependent experiments,
each test signer's performance is itself an average over the 8 test
folds for that signer.

As shown in the first line of Tab.~\ref{t:LER}, the signer-independent
performance of the three types of recognizers is quite poor, with the
rescoring SCRF somewhat outperforming the tandem HMM and first-pass
SCRF.  The poor performance is perhaps to be expected with such a
small number of training signers.

\subsubsection{Signer-adapted recognition}
\label{sec:sigadap_exp}

The remainder of Tab.~\ref{t:LER} (second and third rows) gives the connected FS-letter
recognition performance obtained with the three types of models
using DNNs adapted via fine-tuning, using different types of
adaptation data (ground-truth, GT, vs.~forced-aligned, FA).  For all
models, we do not retrain the models with the adapted DNNs, but tune
hyperparameters\footnote{See~\cite{kim2012,kim2013,tang2015}
  for details of the tuning parameters.} on 10\% of the test signer's data.  The tuned models
are evaluated on an unseen 10\% of the test signer's remaining data;
finally, we repeat this for eight choices of tuning and test sets,
covering the 80\% of the test signer's data that we do not use for
adaptation, and report the mean FS-letter accuracy over the test sets.

As shown in Tab.~\ref{t:LER}, adaptation improves the performance
to up to 30.3\% letter error rate with forced-alignment adaptation
labels and up to 17.3\% letter error rate with ground-truth adaptation labels.
All of the adapted models improve similarly.  However, interestingly, the
first-pass SCRF is slightly worse than the others before adaptation
but better (by 4.4\% absolute) after ground-truth adaptation.  One
hypothesis is that the first-pass SCRF is more dependent on the DNN
performance, while the tandem model uses the original image features
and the rescoring SCRF uses that tandem model's hypotheses.
Once the DNNs are adapted, however, the first-pass SCRF outperforms
the other models.  Fig.~\ref{f:ROAD} shows an example fingerspelling sequence and the hypotheses of the tandem, rescoring SCRF, and first-pass SCRF.

\begin{figure}\centering
\centering
\includegraphics[width=1.0\linewidth]{ROAD}
\caption[Example frame sequence with ground-truth and hypothesized labels and segmentations.]{Sample frames from the word ROAD, with asterisks denoting the peak frame for each FS-letter and ``\texttt{$<$s$>$}'' and ``\texttt{$<$/s$>$}'' denoting periods before the first FS-letter and after the last FS-letter; ground-truth labeling and segmentation based on peak annotations (GT); hypothesized output from the tandem HMM (Tandem), rescoring SCRF (re-SCRF), and first-pass SCRF (1p-SCRF).}
\label{f:ROAD}
\end{figure}

\subsection{Extensions and analysis}

We next analyze our results and consider potential extensions for
improving the models.

\subsubsection{Analysis: Could we do better by training entirely on adaptation data?}
\label{sec:scratch}

Until now we have considered adapting the DNNs while using sequence models (HMMs/SCRFs) trained
only on signer-independent data.
In this section we consider alternatives to this adaptation setting.
We fix the model to a first-pass
SCRF and the adaptation data to 20\% of the test signer's data
annotated with ground-truth labels.  In this setting, we consider
two alternative ways of using the adaptation data:  (1) using the adaptation data from the
test signer to train both the DNNs and sequence model
from scratch, ignoring the signer-independent training set; and (2)
training the DNNs from scratch on the adaptation data, but using the
SCRF trained on the training signers. We compare these options with
our best results using the signer-independent SCRF and DNNs fine-tuned
on the adaptation data.  The results are shown in Tab.~\ref{t:LER_gt}.

We find that ignoring the signer-independent training set and training
both DNNs and SCRFs from scratch on the test signer
(option (1) above) works remarkably well, better than the
signer-independent models and even better than adaptation via
forced alignment (see Tab.~\ref{t:LER}).
However, training the
SCRF on the training signers but DNNs from scratch on the adaptation
data (option (2) above) improves performance further.  However,
neither of these outperforms our previous best approach of
signer-independent SCRFs plus DNNs fine-tuned on the adaptation data.

\begin{table*}[ht!]
\centering
\resizebox{\linewidth}{!}{
\begin{tabular}{|c|c|c|c|c|c||c|c|c|c|c||c|c|c|c|c|}
\hline
& \multicolumn{5}{|c||}{SCRF + DNNs trained from scratch} & \multicolumn{5}{c||}{Sig.-indep. SCRF, DNNs from scratch} & \multicolumn{5}{c|}{Sig.-indep. SCRF, fine-tuned DNN} \\ \hline
Signer & 1 & 2 & 3 & 4 & {\bf Mean} & 1 & 2 & 3 & 4 & {\bf Mean} & 1 & 2 & 3 & 4 & {\bf Mean} \\ \hline\hline
Accuracy & 23.2 &  18.2 &  28.9 &  30.1 & {\bf 25.1} & 18.4 &  12.6 &  27.0 &  20.7 & {\bf 19.7}  & 15.2&   10.6&   24.9&   18.4&   {\bf 17.3} \\ \hline
\end{tabular}
}
\vspace{-.1in}
\caption{Letter error rates (\%) for different settings of SCRF and DNN
  training in the signer-adapted case.  Details are given in Section~\ref{sec:scratch}.}
\label{t:LER_gt}
%\end{center}
\end{table*}


\subsubsection{Analysis:  FS-letter vs.~feature DNNs}

We next compare the FS-letter DNN classifiers and the phonological
feature DNN classifiers in the context of the first-pass SCRF
recognizers.  We also consider an alternative sub-letter feature set,
in particular a set of phonetic features introduced by Keane \cite{Keane2014diss}, whose feature values are listed in Section~\ref{sec:app}, Tab.~\ref{t:phonetic}.  We use the first-pass SCRF
with either only FS-letter classifiers, only phonetic feature
classifiers, FS-letter + phonological feature classifiers, and FS-letter +
phonetic feature classifiers.  We do not consider the case of
phonological features alone, because they are not discrimative for
some FS-letters. Fig.~\ref{fig:ling_fe} shows the FS-letter recognition results
for the signer-dependent and signer-adapted settings.  

We find that using FS-letter classifiers alone outperforms the other
options in the signer-dependent setting, achieving 7.7\% letter error rate.  For signer-adapted
recognition, phonological or phonetic features are helpful in addition
to FS-letters for two of the signers (signers 2 and 4) but not for the
other two (signers 1,3); on average, using FS-letter classifiers alone is
best in both cases, achieving 16.6\% accuracy on average.  In contrast, in earlier work~\cite{kim2012} we found that phonological features 
  outperform FS-letters in the tandem HMM.  However, those experiments
  used a tandem model with neural networks with
  a single hidden layer; we conjecture that with more layers, we are
  able to do a 
  better job at the more complicated task of FS-letter classification.

\begin{figure*}[h!]
\centering
\begin{tabular}{cc}
  \includegraphics[width=0.8\linewidth]{ling_fe_dep}\\
  \includegraphics[width=0.8\linewidth]{ling_fe_indep_adapt} \\
\end{tabular}
\caption{(Top) Signer-dependent recognition and (bottom) signer-independent recognition with frame annotations and adaptation. We compare: FS-letter only, phonetic features only, FS-letters + phonological features \cite{bren} and FS-letters + phonetic features \cite{Keane2014diss}.} 
\label{fig:ling_fe}
\end{figure*}

\input{cnn}

\subsubsection{Improving performance in the force-aligned adaptation case}

We next attempt to improve the performance of adaptation in the
absence of ground-truth (manually annotated) frame labels.  This is an important setting, since in practice it can be very difficult to obtain ground-truth labels at the frame level.  Using only
the FS-letter label sequence for the adaptation data, we use the
signer-independent tandem recognizer to get force-aligned frame
labels.  We then adapt (fine-tune) the DNNs using the force-aligned
adaptation data (as before in the FA case).  We then
re-align the test signer's adaptation data with the adapted
recognizer. Finally, we adapt the DNNs again with the re-aligned data.
Throughout this experiment, we do not change the recognizer but only
update the DNNs.  Using this iterative realignment approach, we are able to furthur improve
the recognition accuracy in the FA case by about 1.3\%, as shown in Tab.~\ref{t:LER_fa}.


\begin{table*}[ht!]
\centering
\resizebox{\linewidth}{!}{
\begin{tabular}{|c|c|c|c|c||c|c|c|c|c|}
\hline
\multicolumn{5}{|c||}{Fine-tuning with FA} &
\multicolumn{5}{c|}{Fine-tuning with FA + realignment} \\ \hline
Signer 1 & Signer 2 & Signer 3 & Signer 4 & {\bf Mean} & Signer 1 & Signer 2 & Signer 3 & Signer 4 & {\bf Mean} \\ \hline\hline
22.7 & 26.0 & 33.4&  34.8&   {\bf 29.2}&  21.9& 25.3&  30.5&  34.0& {\bf 27.9} \\ \hline
\end{tabular}
}
\vspace{-.1in}
\caption{Letter error rates (\%) with iterated forced-alignment (FA) adaptation.}
\label{t:LER_fa}
%\end{center}
\end{table*}

\subsubsection{Improving performance with segmental cascades}

Finally, we consider whether we can improve upon the performance of
our best models, the first-pass SCRFs, by rescoring their results in a second pass with
more powerful features.  We follow the discriminative segmental
cascades (DSC) approach of \cite{tang2015}, where a simpler first-pass SCRF is used for lattice
generation and a second SCRF, with more computationally demanding
features, is used for rescoring.  

For these experiments we start with the most successful first-pass
SCRF in the above experiments, which uses FS-letter DNNs and is adapted
with 20\% of the test signer's data with ground-truth peak handshape labels.  For the second-pass SCRF, we use the
first-pass score as a feature, and add to it two more complex
features:  a segmental DNN, which takes as input an entire
hypothesized segment and produces posterior probabilities for all of
the FS-letter classes; and the ``peak detection'' feature described in
Section~\ref{sec:method}. We use the same bigram language model as in the tandem HMM and rescoring SCRF models. For the segmental DNN, the training segments are given by the ground-truth segmentations derived from manual peak annotations.  The input layer consists of a concatenation of three mean HOG vectors, each averaged over one third of the segment.  We use the same DNN structure and learning strategy as for the DNN frame classifiers.

As shown in Tab.~\ref{t:DSC}, this approach
slightly improves the average FS-letter accuracy over four signers, from
16.6\% in the first pass to 16.2\% in the second pass.  This
improvement, while small, is statistically significant at the p=0.05
level (using the MAPSSWE significance test from the NIST Scoring Toolkit \cite{sctk}). These results combine our most successful ideas and form our final best results for signer-adapted recognition.  For the signer-dependent setting, this approach achieves comparable performance to the first-pass SCRF.

%\kl{and language model?  any other
%  features? also, need more detail about the segmental DNN -- its
  %structure, how it goes from arbitrary length segments to
  %fixed-length vectors}
  
\begin{table*}[ht!]
\centering
\resizebox{\linewidth}{!}{
\begin{tabular}{|c|c|c|c|c||c|c|c|c|c|}
\hline
\multicolumn{5}{|c||}{Signer-dependent} &
\multicolumn{5}{c|}{Signer-independent} \\ \hline
Signer 1 & Signer 2 & Signer 3 & Signer 4 & {\bf Mean} & Signer 1 & Signer 2 & Signer 3 & Signer 4 & {\bf Mean} \\ \hline\hline
7.2& 6.5& 8.1&8.6&    {\bf 7.6}&  13.0& 11.2&  21.7&  18.8& {\bf 16.2} \\ \hline
\end{tabular}
}
\vspace{-.1in}
\caption{Letter error rates (\%) obtained with a two-pass segmental cascade.}
\label{t:DSC}
%\end{center}
\end{table*}

%\kl{insert table, if possible with signer-dep results too}


